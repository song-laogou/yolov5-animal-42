# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import os
import os.path as osp
import cv2
img_files = os.listdir("../zebra")
for i, img_file in enumerate(img_files):
    # print(img_file)
    try:
        img_path = osp.join("../zebra", img_file)
        img = cv2.imread(img_path)
        cv2.imwrite("../images_zebra/{}.jpg".format(i), img)
    except:
        print(img_file)

