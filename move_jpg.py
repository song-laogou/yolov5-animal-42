#!/usr/bin/env python
# -*- coding: UTF-8 -*-
'''
@Project ：yolov5-car 
@File    ：move_jpg.py
@Author  ：ChenmingSong
@Date    ：2022/3/4 23:37 
@Description：
'''
import os
import shutil
import os.path as osp

files = os.listdir("F:/bbbbbbbbbb/tmp/zrbra/labels")
for file in files:
    file_name = file.split(".")[0]
    file_path = osp.join("F:/bbbbbbbbbb/tmp/zrbra/images", file_name + ".jpg")
    shutil.copy2(file_path, "F:/bbbbbbbbbb/tmp/zrbra/JPEGImages")